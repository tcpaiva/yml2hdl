import random

from _common import *
from _helpers import *

__ = Y2H.TAB


def format_sv_type(ctype, indent=1):
    i = indent

    if ctype.pkd:        
        svh.append(f"\n{__*i}typedef struct packed {{")
    else:
        svh.append(f"\n{__*i}typedef struct {{")
    for k, v in ctype.members.items():
        if v.range is not None:
            if isinstance(v.range, str) and v.range.lower() == "closed":
                r = ""
            elif isinstance(v.range, str) and v.range.lower() == "open":
                r = []
            elif isinstance(v.range, list) and len(v.range) == 2:
                r = f"[{v.range[0]}:{v.range[1]}]"
            else:
                eprint(f"ERROR: invalid range for '{ctype.name}.{k}'")
        else:
            r  = ""

        t = v.type.sv

        svh.append(f"{__*i}{__}{t} {r} {k};")
    svh.append(f"{__*i}}} {ctype.name} ;")


def format_vhdl_type(ctype, indent=1, compat=False):
    i = indent

    vhdl.decl.append(f"")
    if ctype.comment != "":
        for line in ctype.comment.splitlines():
            vhdl.decl.append(f"{__*i}-- {ctype.name}: {line}")



    vhdl.decl.append(f"{__*i}type {ctype.name} is record")
    for k, v in ctype.members.items():
        if hasattr(v, 'range') and v.range is not None:
            if isinstance(v.range, str) and v.range.lower() == "open":
                if v.type.name in basic_vectors:
                    r = ""
                else:
                    r = f"(integer range <>)"
            elif isinstance(v.range, list) and len(v.range) == 2:
                if evaluate(v.range[0]) <= evaluate(v.range[1]):
                    r = f"({v.range[0]} to {v.range[1]})"
                else:
                    r = f"({v.range[0]} downto {v.range[1]})"
            elif isinstance(v.range, str) and v.range.lower() == "closed":
                r = ""
            else:
                eprint(f"ERROR: invalid range for '{ctype.name}.{k}'")
        else:
            r  = ""

        if hasattr(v, 'array') and v.array is not None:
            if isinstance(v.array, str) and v.array.lower() == "open":
                if v.type.name in basic_vectors:
                    a = ""
                else:
                    a = f"(integer range <>)"
            elif isinstance(v.array, list) and len(v.array) == 2:
                if evaluate(v.array[0]) <= evaluate(v.array[1]):
                    a = f"({v.array[0]} to {v.array[1]})"
                else:
                    a = f"({v.array[0]} downto {v.array[1]})"
            else:
                eprint(f"ERROR: invalid array for '{ctype.name}.{k}'")
        else:
            a  = ""

        if isinstance(v.type, Array):
            t = v.type.vhdl[1]
        elif isinstance(v.type, Record):
            t = v.type.vhdl[0]
        else:
            t = v.type.vhdl[0] if v.range is None else v.type.vhdl[1]

        if v.comment != "":
            for line in v.comment.splitlines():
                vhdl.decl.append(f"{__*i}{__}-- {v.name}: {line}")
        vhdl.decl.append(f"{__*i}{__}{k} : {t}{a}{r};")
        
    vhdl.decl.append(f"{__*i}end record {ctype.name};")
    if ctype.bits is not None:
        vhdl.decl.append(f"{__*i}attribute w of {ctype.name} : type is {ctype.bits};")

def format_vhdl_width_function(ctype, indent, fname):
    i = indent

    # width ------------------------------------------------------------
    s = f"function {fname}(x: {ctype.vhdl[0]}) return natural"
    vhdl.decl.append(    f"{__*i}{s};")
    vhdl.body.append(    f"\n{__*i}{s} is")
    vhdl.body.append(    f"{__*i}{__}variable w : natural := 0;")
    vhdl.body.append(    f"{__*i}begin")
    if Y2H.debug:
        vhdl.body.append(    f"{__*i}{__}report \"... size of {ctype.name}\";")
    for n, e in ctype.members.items():
        vhdl.body.append(f"{__*i}{__}w := w + {fname}(x.{n});")
    vhdl.body.append(    f"{__*i}{__}return w;")
    vhdl.body.append(    f"{__*i}end function {fname};")


def format_vhdl_vectorify_function(ctype, indent, fname):
    i = indent

    if fname == 'convert':
        width_func = 'width'
    else:
        width_func = 'len'

    # std_logic_vector <- record ---------------------------------------
    s = f"function {fname}(x: {ctype.vhdl[0]}; tpl: std_logic_vector) return std_logic_vector"
    vhdl.decl.append(    f"{__*(i)}{s};")
    vhdl.body.append(    f"{__*(i)}{s} is")
    vhdl.body.append(    f"{__*i}{__}variable y : std_logic_vector(tpl'range);")
    vhdl.body.append(    f"{__*i}{__}variable w : integer;")
    vhdl.body.append(    f"{__*i}{__}variable u : integer := tpl'left;")
    vhdl.body.append(    f"{__*(i)}begin")
    if Y2H.debug:
        vhdl.body.append(    f"{__*i}{__}report \"... flattening {ctype.name}\";")
    vhdl.body.append(    f"{__*i}{__}if tpl'ascending then")
    for n, e in ctype.members.items():
        vhdl.body.append(f"{__*i}{__}{__}w := {width_func}(x.{n});")
        vhdl.body.append(f"{__*i}{__}{__}y(u to u+w-1) := {fname}(x.{n}, y(u to u+w-1));")
        if n == list(ctype.members)[-1]: break
        vhdl.body.append(f"{__*i}{__}{__}u := u + w;")
    vhdl.body.append(    f"{__*i}{__}else")
    for n, e in ctype.members.items():
        vhdl.body.append(f"{__*i}{__}{__}w := {width_func}(x.{n});")
        vhdl.body.append(f"{__*i}{__}{__}y(u downto u-w+1) := {fname}(x.{n}, y(u downto u-w+1));")
        if n == list(ctype.members)[-1]: break
        vhdl.body.append(f"{__*i}{__}{__}u := u - w;")
    vhdl.body.append(    f"{__*i}{__}end if;")
    vhdl.body.append(    f"{__*i}{__}return y;")
    vhdl.body.append(    f"{__*(i)}end function {fname};")


def format_vhdl_structify_function(ctype, indent, fname):
    i = indent

    if fname == 'convert':
        width_func = 'width'
    else:
        width_func = 'len'

    constraining = ""
    for member_name, member_type in ctype.members.items():
        if member_type.bits is None:

            if isinstance(member_type, Array):
                if isinstance(member_type.range, list):
                    r = ""
                elif isinstance(member_type.range, str):
                    r = "({member_type.name}({member_type.name}'low)'range)"
                else:
                    etype = member_type.type
                    while isinstance(etype, Array):
                        etype = etype.type

                    if etype.bits is None:
                        r = "({member_type.name}({member_type.name}'low)'range)"
                    else:
                        r = ""

                a = "({member_type.name}'range)"
                ttype = member_type
                while isinstance(ttype, Array) and not isinstance(ttype.array, list):
                    ttype = ttype.type
                if isinstance(ttype, Array) and isinstance(ttype.array, list):
                    a = ""
                else:
                    a = "({member_type.name}'range)"
            else:
                a = ""
                r = f"(tpl.{member_name}'range)" if not isinstance(member_type.range, list) else ""

            constraining += f"{member_name}{a}{r},"

            if constraining != "":
                constraining = f"({constraining[:-1]})"

    # record <- std_logic_vector ---------------------------------------
    s = f"function {fname}(x: std_logic_vector; tpl: {ctype.vhdl[0]}) return {ctype.vhdl[0]}"
    vhdl.decl.append(    f"{__*(i)}{s};")
    vhdl.body.append(    f"{__*(i)}{s} is")
    vhdl.body.append(    f"{__*i}{__}variable y : {ctype.vhdl[0]}{constraining};")
    vhdl.body.append(    f"{__*i}{__}variable w : integer;")
    vhdl.body.append(    f"{__*i}{__}variable u : integer := x'left;")
    vhdl.body.append(    f"{__*(i)}begin")
    if Y2H.debug:
        vhdl.body.append(    f"{__*i}{__}report \"... structuring {ctype.name}\";")
    vhdl.body.append(    f"{__*i}{__}if x'ascending then")
    for n, e in ctype.members.items():
        vhdl.body.append(f"{__*i}{__}{__}w := {width_func}(tpl.{n});")
        vhdl.body.append(f"{__*i}{__}{__}y.{n} := {fname}(x(u to u+w-1), tpl.{n});")
        if n == list(ctype.members)[-1]: break
        vhdl.body.append(f"{__*i}{__}{__}u := u + w;")
    vhdl.body.append(    f"{__*i}{__}else")
    for n, e in ctype.members.items():
        vhdl.body.append(f"{__*i}{__}{__}w := {width_func}(tpl.{n});")
        vhdl.body.append(f"{__*i}{__}{__}y.{n} := {fname}(x(u downto u-w+1), tpl.{n});")
        if n == list(ctype.members)[-1]: break
        vhdl.body.append(f"{__*i}{__}{__}u := u - w;")
    vhdl.body.append(    f"{__*i}{__}end if;")
    vhdl.body.append(    f"{__*i}{__}return y;")
    vhdl.body.append(    f"{__*(i)}end function {fname};")


def format_vhdl_zero_function(ctype, indent, fname):

    ii = indent

    if fname == 'zero':
        width_func = 'width'
    else:
        width_func = 'len'

    s = f"function {fname}(tpl: {ctype.name}) return {ctype.name}"
    vhdl.decl.append(    f"{__*ii}{s};")
    vhdl.body.append(    f"{__*ii}{s} is")
    vhdl.body.append(    f"{__*ii}begin")
    if Y2H.debug:
        vhdl.body.append(f"{__*ii}{__}report \"... zeroing {ctype.name}\";")
    vhdl.body.append(    f"{__*ii}{__}return convert(std_logic_vector'({width_func}(tpl)-1 downto 0 => '0'), tpl);")
    vhdl.body.append(    f"{__*ii}end function {fname};")


def format_vhdl_functions(ctype, indent=1, compat=False):
    if compat == 0 or compat == 2:
        format_vhdl_width_function(ctype, indent, "width")
        format_vhdl_vectorify_function(ctype, indent, "convert")
        format_vhdl_structify_function(ctype, indent, "convert")
        format_vhdl_zero_function(ctype, indent, "zero")
    if compat:
        format_vhdl_width_function(ctype, indent, "len")
        format_vhdl_vectorify_function(ctype, indent, "vectorify")
        format_vhdl_structify_function(ctype, indent, "structify")
        format_vhdl_zero_function(ctype, indent, "nullify")

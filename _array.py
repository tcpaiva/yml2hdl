from _common import *
from _helpers import *

__ = Y2H.TAB


def format_sv_type(ctype, indent=1):
    ii = indent

    if ctype.array == "open":
        a = f'[]'
    elif isinstance(ctype.array, list) and len(ctype.array) == 2:
        a = f"[{ctype.array[0]}:{ctype.array[1]}]"
    else:
        print(f"ERROR: invalid array range for '{ctype.name}'")
        sys.exit(1)

    if ctype.range is None or (isinstance(ctype.range, str) and ctype.range.lower() == "closed"):
        e = ""
    elif ctype.range == "open":
        e = f'[]'
    elif isinstance(ctype.range, list) and len(ctype.range) == 2:
        e = f"[{ctype.range[0]}:{ctype.range[1]}]"
    else:
        print(f"ERROR: invalid type range in array '{ctype.name}'")
        sys.exit(1)

    t = ctype.type.sv

    if ctype.pkd:
        svh.append(f"\n{__*ii}typedef {ctype.type.sv} {e} {a}{ctype.name};")
    else:
        svh.append(f"\n{__*ii}typedef {ctype.type.sv}{e} {ctype.name}{a};")


def format_vhdl_type(ctype, indent=1, compat=False):
    ii = indent

    vhdl.decl.append(f"")
    if ctype.comment != "":
        for line in ctype.comment.splitlines():
            vhdl.decl.append(f"{__*ii}-- {ctype.name}: {line}")
    
    if ctype.array == "open":
        a = f'(integer range <>)'
    elif isinstance(ctype.array, list) and len(ctype.array) == 2:
        if evaluate(ctype.array[0]) <= evaluate(ctype.array[1]):
            a = f"({ctype.array[0]} to {ctype.array[1]})"
        else:
            a = f"({ctype.array[0]} downto {ctype.array[1]})"
    else:
        print(f"ERROR: invalid array range for '{ctype.name}'")
        sys.exit(1)

    if ((ctype.range is None) or (ctype.range == "closed")
        or (ctype.range == 'open' and ctype.type.name in basic_vectors)):
        e = ""
    elif ctype.range == "open":
        e = f''
    elif isinstance(ctype.range, list) and len(ctype.range) == 2:
        if evaluate(ctype.range[0]) <= evaluate(ctype.range[1]):
            e = f"({ctype.range[0]} to {ctype.range[1]})"
        else:
            e = f"({ctype.range[0]} downto {ctype.range[1]})"
    else:
        print(f"ERROR: invalid type range in array '{ctype.name}'")
        sys.exit(1)

    if isinstance(ctype.type, Array) or ctype.type.name in basic_vectors:
        t = ctype.type.vhdl[1]
    else:
        t = ctype.type.vhdl[0]

    if ctype.qualifier.lower() == 'subtype':
        vhdl.decl.append(f"{__*ii}subtype {ctype.name} is {t}{a}{e};")
    else:
        vhdl.decl.append(f"{__*ii}type {ctype.name} is array{a} of {t}{e};")

    if ctype.bits is not None:
        vhdl.decl.append(f"{__*ii}attribute w of {ctype.name} : {ctype.qualifier} is {ctype.bits};")



def format_vhdl_width(ctype, indent, fname):

    if ctype.qualifier.lower() == 'subtype': return

    ii = indent

    s = f"function {fname}(x: {ctype.vhdl[1]}) return integer"
    vhdl.decl.append(f"{__*ii}{s};")
    vhdl.body.append(f"\n{__*ii}{s} is")
    # vhdl.body.append(f"{__*ii}{__}variable w : integer := x'length * {fname}(x(x'low));")
    vhdl.body.append(f"{__*ii}{__}variable w : integer;")
    vhdl.body.append(f"{__*ii}begin")
    if Y2H.debug:
        vhdl.body.append(f"{__*ii}{__}report \"... size of {ctype.name}\";")
    vhdl.body.append(f"{__*ii}{__}if x'length < 1 then")
    vhdl.body.append(f"{__*ii}{__}  w := 0;")
    vhdl.body.append(f"{__*ii}{__}else")
    vhdl.body.append(f"{__*ii}{__}  w := x'length * width(x(x'low));")
    vhdl.body.append(f"{__*ii}{__}end if;")
    vhdl.body.append(f"{__*ii}{__}return w;")
    vhdl.body.append(f"{__*ii}end function {fname};")


def format_vhdl_vectorify(ctype, indent, fname):

    if ctype.qualifier.lower() == 'subtype': return

    ii = indent

    if fname == 'convert':
        width_func = 'width'
    else:
        width_func = 'len'

    # std_logic_vector <- array ----------------------------------------
    s = f"function {fname}(x: {ctype.vhdl[1]}; tpl: std_logic_vector) return std_logic_vector"
    vhdl.decl.append(f"{__*ii}{s};")
    vhdl.body.append(f"{__*ii}{s} is")
    vhdl.body.append(f"{__*ii}{__}variable y : std_logic_vector(tpl'range);")
    vhdl.body.append(f"{__*ii}{__}constant W : natural := {width_func}(x(x'low));")
    vhdl.body.append(f"{__*ii}{__}variable a : integer;")
    vhdl.body.append(f"{__*ii}{__}variable b : integer;")
    vhdl.body.append(f"{__*ii}begin")
    if Y2H.debug:
        vhdl.body.append(f"{__*ii}{__}report \"... flattening {ctype.name}\";")
    vhdl.body.append(f"{__*ii}{__}if y'ascending then")
    vhdl.body.append(f"{__*ii}{__}{__}for i in 0 to x'length-1 loop")
    vhdl.body.append(f"{__*ii}{__}{__}{__}a := W*i + y'low + W - 1;")
    vhdl.body.append(f"{__*ii}{__}{__}{__}b := W*i + y'low;")
    vhdl.body.append(f"{__*ii}{__}{__}{__}assign(y(b to a), {fname}(x(i+x'low), y(b to a)));")
    vhdl.body.append(f"{__*ii}{__}{__}end loop;")
    vhdl.body.append(f"{__*ii}{__}else")
    vhdl.body.append(f"{__*ii}{__}{__}for i in 0 to x'length-1 loop")
    vhdl.body.append(f"{__*ii}{__}{__}{__}a := W*i + y'low + W - 1;")
    vhdl.body.append(f"{__*ii}{__}{__}{__}b := W*i + y'low;")
    vhdl.body.append(f"{__*ii}{__}{__}{__}assign(y(a downto b), {fname}(x(i+x'low), y(a downto b)));")
    vhdl.body.append(f"{__*ii}{__}{__}end loop;")
    vhdl.body.append(f"{__*ii}{__}end if;")
    vhdl.body.append(f"{__*ii}{__}return y;")
    vhdl.body.append(f"{__*ii}end function {fname};")


def format_vhdl_structify(ctype, indent, fname):

    if ctype.qualifier.lower() == 'subtype': return

    ii = indent

    if isinstance(ctype.range, list):
        r = ""
    elif isinstance(ctype.range, str):
        r = "(tpl(tpl'low)'range)"
    else:
        etype = ctype.type
        while isinstance(etype, Array):
            etype = etype.type

        if etype.bits is None:
            r = "(tpl(tpl'low)'range)"
        else:
            r = ""

    a = "(tpl'range)"
    ttype = ctype
    while isinstance(ttype, Array) and not isinstance(ttype.array, list):
        ttype = ttype.type

    if isinstance(ttype, Array) and isinstance(ttype.array, list):
        a = ""
    else:
        a = "(tpl'range)"

    if fname == 'convert':
        width_func = 'width'
    else:
        width_func = 'len'

    # array <- std_logic_vector ---------------------------------------
    s = f"function {fname}(x: std_logic_vector; tpl: {ctype.name}) return {ctype.name}"
    vhdl.decl.append(    f"{__*ii}{s};")
    vhdl.body.append(    f"{__*ii}{s} is")
    vhdl.body.append(    f"{__*ii}{__}variable y : {ctype.name}{a}{r};")
    vhdl.body.append(    f"{__*ii}{__}constant W : natural := {width_func}(y(y'low));")
    vhdl.body.append(    f"{__*ii}{__}variable a : integer;")
    vhdl.body.append(    f"{__*ii}{__}variable b : integer;")
    vhdl.body.append(    f"{__*ii}begin")
    if Y2H.debug:
        vhdl.body.append(f"{__*ii}{__}report \"... structuring {ctype.name}\";")
    vhdl.body.append(    f"{__*ii}{__}if x'ascending then")
    vhdl.body.append(    f"{__*ii}{__}{__}for i in 0 to y'length-1 loop")
    vhdl.body.append(    f"{__*ii}{__}{__}{__}a := W*i + x'low + W - 1;")
    vhdl.body.append(    f"{__*ii}{__}{__}{__}b := W*i + x'low;")
    vhdl.body.append(    f"{__*ii}{__}{__}{__}y(i+y'low) := {fname}(x(b to a), y(i+y'low));")
    vhdl.body.append(    f"{__*ii}{__}{__}end loop;")
    vhdl.body.append(    f"{__*ii}{__}else")
    vhdl.body.append(    f"{__*ii}{__}{__}for i in 0 to y'length-1 loop")
    vhdl.body.append(    f"{__*ii}{__}{__}{__}a := W*i + x'low + W - 1;")
    vhdl.body.append(    f"{__*ii}{__}{__}{__}b := W*i + x'low;")
    vhdl.body.append(    f"{__*ii}{__}{__}{__}y(i+y'low) := {fname}(x(a downto b), y(i+y'low));")
    vhdl.body.append(    f"{__*ii}{__}{__}end loop;")
    vhdl.body.append(    f"{__*ii}{__}end if;")
    vhdl.body.append(    f"{__*ii}{__}return y;")
    vhdl.body.append(    f"{__*ii}end function {fname};")


def format_vhdl_zero(ctype, indent, fname):

    if ctype.qualifier.lower() == 'subtype': return

    ii = indent

    if isinstance(ctype.range, list):
        r = ""
    elif isinstance(ctype.range, str):
        r = "(tpl(tpl'low)'range)"
    else:
        etype = ctype.type
        while isinstance(etype, Array):
            etype = etype.type

        if etype.bits is None:
            r = "(tpl(tpl'low)'range)"
        else:
            r = ""

    a = "(tpl'range)"
    ttype = ctype
    while isinstance(ttype, Array) and not isinstance(ttype.array, list):
        ttype = ttype.type
    if isinstance(ttype, Array) and isinstance(ttype.array, list):
        a = ""
    else:
        a = "(tpl'range)"

    if fname == 'zero':
        width_func = 'width'
    else:
        width_func = 'len'

    s = f"function {fname}(tpl: {ctype.name}) return {ctype.name}"
    vhdl.decl.append(    f"{__*ii}{s};")
    vhdl.body.append(    f"{__*ii}{s} is")
    vhdl.body.append(    f"{__*ii}begin")
    if Y2H.debug:
        vhdl.body.append(f"{__*ii}{__}report \"... zeroing {ctype.name}\";")
    vhdl.body.append(    f"{__*ii}{__}return convert(std_logic_vector'({width_func}(tpl)-1 downto 0 => '0'), tpl);")
    vhdl.body.append(    f"{__*ii}end function {fname};")


def format_vhdl_vectorify_varray(ctype, indent, fname):
    # std_logic_vector_array <= array ------------------------------------------

    if ctype.qualifier.lower() == 'subtype': return

    ii = indent

    s = f"function {fname}(x: {ctype.name}; tpl: std_logic_vector_array) return std_logic_vector_array"
    vhdl.decl.append(    f"{__*ii}{s};")
    vhdl.body.append(    f"{__*ii}{s} is")
    vhdl.body.append(    f"{__*ii}{__}variable y : std_logic_vector_array(tpl'range)(tpl(tpl'low)'range);")
    vhdl.body.append(    f"{__*ii}begin")
    if Y2H.debug:
        vhdl.body.append(f"{__*ii}{__}report \"... flattening varray {ctype.name}\";")
    vhdl.body.append(    f"{__*ii}{__}for j in y'range loop")
    vhdl.body.append(    f"{__*ii}{__}    y(j) := {fname}(x(j), (y(j)'range => '0'));")
    vhdl.body.append(    f"{__*ii}{__}end loop;")
    vhdl.body.append(    f"{__*ii}{__}return y;")
    vhdl.body.append(    f"{__*ii}end function {fname};")


def format_vhdl_structify_varray(ctype, indent, fname):
    # array <= std_logic_vector_array ------------------------------------------

    if ctype.qualifier.lower() == 'subtype': return

    if isinstance(ctype.range, list):
        r = ""
    elif isinstance(ctype.range, str):
        r = "(tpl(tpl'low)'range)"
    else:
        etype = ctype.type
        while isinstance(etype, Array):
            etype = etype.type

        if etype.bits is None:
            r = "(tpl(tpl'low)'range)"
        else:
            r = ""

    a = "(tpl'range)"
    ttype = ctype
    while isinstance(ttype, Array) and not isinstance(ttype.array, list):
        ttype = ttype.type

    if isinstance(ttype, Array) and isinstance(ttype.array, list):
        a = ""
    else:
        a = "(tpl'range)"

    ii = indent

    s = f"function {fname}(x: std_logic_vector_array; tpl: {ctype.name}) return {ctype.name}"
    vhdl.decl.append(    f"{__*ii}{s};")
    vhdl.body.append(    f"{__*ii}{s} is")
    vhdl.body.append(    f"{__*ii}{__}variable y : {ctype.name}{a}{r};")
    vhdl.body.append(    f"{__*ii}begin")
    if Y2H.debug:
        vhdl.body.append(f"{__*ii}{__}report \"... structuring varray {ctype.name}\";")
    vhdl.body.append(    f"{__*ii}{__}for j in y'range loop")
    vhdl.body.append(    f"{__*ii}{__}    y(j) := {fname}(x(j), y(j));")
    vhdl.body.append(    f"{__*ii}{__}end loop;")
    vhdl.body.append(    f"{__*ii}{__}return y;")
    vhdl.body.append(    f"{__*ii}end function {fname};")

def format_vhdl_functions(ctype, indent=1, compat=False):
    if compat == 0 or compat == 2:
        format_vhdl_width(ctype, indent, "width")
        format_vhdl_vectorify(ctype, indent, "convert")
        format_vhdl_structify(ctype, indent, "convert")
        format_vhdl_zero(ctype, indent, "zero")
        format_vhdl_vectorify_varray(ctype, indent, "convert")
        format_vhdl_structify_varray(ctype, indent, "convert")
    if compat:
        format_vhdl_width(ctype, indent, "len")
        format_vhdl_vectorify(ctype, indent, "vectorify")
        format_vhdl_structify(ctype, indent, "structify")
        format_vhdl_zero(ctype, indent, "nullify")
        format_vhdl_vectorify_varray(ctype, indent, "vectorify")
        format_vhdl_structify_varray(ctype, indent, "structify")

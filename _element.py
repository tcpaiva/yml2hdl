import re

from _common import *
from _helpers import *

TAB = Y2H.TAB

def format_vhdl_type(ctype, indent=1, compat=False):
    ii = indent
    v = ";"
    sep = "is"

    vhdl.decl.append(f"")
    if ctype.comment != "":
        for line in ctype.comment.splitlines():
            vhdl.decl.append(f"{TAB*ii}-- {ctype.name}: {line}")

    if ctype.value is not None:
        q = 'constant'
        sep = ":"

        if isinstance(ctype.type, Element):

            if isinstance(ctype.value, str):
                v = f" := {evaluate(ctype.value, 'vhdl')}; -- {ctype.value}"

            elif isinstance(ctype.value, list):
                if (ctype.range is None and (isinstance(ctype.type.array, str)
                                             and ctype.type.arractype.lower() == "open")):
                    eprint(f"ERROR: '{ctype.name}' is unconstrained.")

                if ctype.range is not None:
                    left = evaluate(ctype.range[0])
                    right = evaluate(ctype.range[1])
                else:
                    left = evaluate(ctype.type.array[0])
                    right = evaluate(ctype.type.array[1])
                if left <= right:
                    r = range(left, right+1, 1)
                else:
                    r = range(left, right-1, -1)
                if len(ctype.value) != len(r):
                    eprint(f"Incompatible number of elements for '{ctype.name}'.")
                v = f' := (\n'
                for j in r:
                    if isinstance(ctype.value[j], str):
                        v += f"{TAB*(ii+1)}{j} => {evaluate(ctype.value[j], 'vhdl')}, -- {ctype.value[j]}\n"
                    else:
                        v += f"{TAB*(ii+1)}{j} => {evaluate(ctype.value[j], 'vhdl')},\n"
                last_comma = v.rfind(',')
                v = v[:last_comma] + v[last_comma+1:]
                v += f'{TAB*(ii)});'
            else:
                v = f' := {ctype.value};'
        elif isinstance(ctype.type, Record):
            v = f' := (\n'
            if len(ctype.type.members) != len(ctype.value):
                eprint(f"Incompatible number of elements for '{ctype.name}'.")
            for j, n in enumerate(ctype.type.members):
                if isinstance(ctype.value[j], str):
                    v += f"{TAB*(ii+1)}{n} => {evaluate(ctype.value[j], 'vhdl')}, -- {ctype.value[j]}\n"
                else:
                    v += f"{TAB*(ii+1)}{n} => {evaluate(ctype.value[j], 'vhdl')},\n"
            last_comma = v.rfind(',')
            v = v[:last_comma] + v[last_comma+1:]
            v += f'{TAB*(ii)});'
        elif isinstance(ctype.type, Array):
            if isinstance(ctype.value, list):
                if (ctype.range is None and (isinstance(ctype.type.array, str)
                                             and ctype.type.arractype.lower() == "open")):
                    eprint(f"ERROR: '{ctype.name}' is unconstrained.")
                if ctype.range is not None:
                    left = evaluate(ctype.range[0])
                    right = evaluate(ctype.range[1])
                else:
                    left = evaluate(ctype.type.array[0])
                    right = evaluate(ctype.type.array[1])
                if left <= right:
                    r = range(left, right+1, 1)
                else:
                    r = range(left, right-1, -1)
                if len(ctype.value) != len(r):
                    eprint(f"Incompatible number of elements for '{ctype.name}'.")
                v = f' := (\n'
                for j in r:
                    v += f"{TAB*(ii+1)}{j} => {ctype.value[j]},\n"
                v = v[:-2] + f'\n{TAB*(ii)});'
            else:
                v = f' := (others => {ctype.value});'
    elif ctype.type.name in basic_types:
        q = 'subtype'
    else:
        q = 'type'

    if ctype.range is None or (ctype.range == 'open' and ctype.type.name.lower() in basic_vectors):
        r = ""
    elif ctype.range == "open":
        r = f'(integer range <>)'
    elif isinstance(ctype.range, list) and len(ctype.range) == 2:

        left = add_width_attributes(ctype.range[0])
        right = add_width_attributes(ctype.range[1])

        if evaluate(ctype.range[0]) <= evaluate(ctype.range[1]):
            r = f"({left} to {right})"
        else:
            r = f"({left} downto {right})"
    else:
        eprint(f"ERROR: invalid range for '{ctype.type.name}'")

    if isinstance(ctype.type, Array):
        t = ctype.type.vhdl[1]
    elif isinstance(ctype.type, Record):
        t = ctype.type.vhdl[0]
    else:
        t = ctype.type.vhdl[0] if ctype.range is None else ctype.type.vhdl[1]

    vhdl.decl.append(f"{TAB*ii}{q} {ctype.name} {sep} {t}{r}{v}")
    if ctype.bits is not None:
        vhdl.decl.append(f"{TAB*ii}attribute w of {ctype.name} : {q} is {ctype.bits};")


def format_sv_type(ctype, indent=1, compat=False):
    i = indent

    svh.append("")

    if ctype.comment:
        for line in ctype.comment.strip().split("\n"):
            svh.append(f'{TAB*i}// {ctype.name}: {line}')
            
    r = ""
    v = ""

    if ctype.value is None:
        q = "typedef "
    else:
        q = "parameter "

    if isinstance(ctype.type, Element):
        if isinstance(ctype.value, str):
            v = [" = "] + list(str(evaluate(ctype.value, 'sv'))) #re.split(f"(?<=[-+*/()])|(?=[-+*/()])", ctype.value)
            n = 0; m = 0
            for j, piece in enumerate(v):
                if piece == "(":
                    m += 1
                elif piece == ")":
                    m -= 1
                if m == 1 and n > 0 and piece == ")":
                    v[j] = ")"*(n+1)
                    n = 0
                    continue
                elif piece in sv_map:
                    v[j] = sv_map[piece][0]
                    n += sv_map[piece][1]
            v = "".join(v)
        elif ctype.value is not None:
            v = f' = {ctype.value}'

        if isinstance(ctype.range, list):
            left = evaluate(ctype.range[0])
            right = evaluate(ctype.range[1])
            r = f"[{left},{right}]"
            
    elif isinstance(ctype.type, Record):
        v = " = '{\n"
        if len(ctype.type.members) != len(ctype.value):
            eprint(f"Incompatible number of elements for '{ctype.name}'.")
        for j, n in enumerate(ctype.type.members):
            v += f"    .{n} => {ctype.value[j]},\n"
        v += '  }'
        r = ""
    elif isinstance(ctype.type, Array):
        if isinstance(ctype.value, list):
            if (ctype.range is None and (isinstance(ctype.type.array, str)
                                         and ctype.type.array.lower() == "open")):
                eprint(f"ERROR: '{ctype.name}' is unconstrained.")
            if isinstance(ctype.range, str) and ctype.range.lower() == "open":
                r = f"[]"
            elif ctype.range is not None:
                left = evaluate(ctype.range[0])
                right = evaluate(ctype.range[1])
                r = f"[{left}, {right}]"
            else:
                r = ""
            v = f"{ctype.value}"
            v = f" = '{{{v[1:-1]}}}"
        elif ctype is not None:
            v = f" = '{{{ctype.value}}}"

    if ctype.pkd:
        svh.append(f"{TAB*i}{q}{ctype.type.sv} {r} {ctype.name}{v};")
    else:
        svh.append(f"{TAB*i}{q}{ctype.type.sv} {ctype.name}{r}{v};")

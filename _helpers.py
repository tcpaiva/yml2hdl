import math
import sys
import yaml
import re

from _common import *


def is_open(arange):
    if isinstance(arange, str) and arange.lower() == "open":
        return True
    else:
        return False

def parse_file(filepath, version, desc):
    if not filepath:
        return {}, {}

    try:
        with open(filepath, 'r') as f:
            tmp  = yaml.safe_load(f)
    except OSError:
        print(f"ERROR: invalid path '{filepath}'.")
    except Exception as e:
        eprint(f"ERROR: malformed file '{filepath}'.\n"
               f"The error message is: {e}.")


    vprint(f"File: {filepath}")

    src = {}
    cnf = {}

    if not isinstance(tmp, dict):
        eprint(f"ERROR: malformed file '{filepath}'.")


    if version == '0.2':
        for x,y in tmp.items():
            if 'config' in x.lower():
                cnf.update(y)
            elif any(u in x.lower() for u in HDL_SECTION_ROOTS):
                for j in y:
                    for ja, jb in j.items(): pass
                    record = []
                    element_array = {}
                    for k in jb:
                        for ka, kb in k.items(): pass
                        if (isinstance(kb, list)
                            and ka.lower() != 'value' and ka.lower() != 'array'
                            and ka.lower() != 'length' and ka.lower() != 'range'):
                            r_item = {}
                            for l in kb:
                                for la, lb in l.items(): pass
                                if la.lower() == 'length':
                                    la = 'range'
                                    if not (isinstance(lb, str) and lb.lower() == "open"):
                                        lb = [f"{lb}-1", 0]
                                if la.lower() == 'array':
                                    if not (isinstance(lb, str) and lb.lower() == "open"):
                                        lb = [f"{lb}-1", 0]
                                if not la.startswith('_'):
                                    r_item.update({la:lb})
                            record.append({ka:r_item})
                        else:
                            if ka.lower() == 'length':
                                ka = 'range'
                                if not (isinstance(kb, str) and kb.lower() == "open"):
                                    kb = [f"{kb}-1", 0]
                            if (ka.lower() == 'array'):
                                if not (isinstance(kb, str) and kb.lower() == "open"):
                                    kb = [f"{kb}-1", 0]
                            if not ka.startswith('_'):
                                element_array.update({ka: kb})
                    if record: src.update({ja:record})
                    if element_array: src.update({ja:element_array})
                    vprint(f"{Y2H.TAB}{desc}: {ja}")

    elif version == '0.3':
        for x,y in tmp.items():
            if 'config' in x.lower():
                cnf.update(y)
            if any(u in x.lower() for u in HDL_SECTION_ROOTS):
                for z in y:
                    for z_ in z: pass
                    if (z_ in list(src)):
                        eprint(f"ERROR: name clash for {z_}")
                    src.update(z)
                    vprint(f"{Y2H.TAB}{desc}: {z_}")
    return src, cnf

def convert_input(version, src, cnf):
    TAB = Y2H.TAB

    if version == '0.3':
        print("config:")
        for x,y in cnf.items():
            if x.lower() == "packages":
                print(f"{TAB*1}packages:",)
                for libs in y:
                    for lib, pkgs in libs.items():
                        print(f"{TAB*2}- {lib}: [{', '.join(pkgs)}]")
            else:
                if x.lower() == "basic_convert_functions":
                    x = "basic_functions"
                print(f"{TAB*1}{x}: {y}")
        print("types:")
        for x,y in src.items():
            if isinstance(y, dict):
                if any(a in allparams for a in y):
                    if ('comment' in y
                        or ('value' in y and isinstance(y['value'], list))):
                        print(f"{TAB*1}- {x}:")
                        for a, b in y.items():
                            if a == 'value' and isinstance(b, list):
                                print(f"{TAB*2}{a}:")
                                for l in b:
                                    print(f"{TAB*3}- {l}")
                            else:
                                print(f"{TAB*2}{a}: {b}")
                    else:
                        print(f"{TAB*1}- {x}: {y}")
            if isinstance(y, list):
                print(f"{TAB*1}- {x}:")
                for w in y:
                    for u,v in w.items():
                        if 'comment' in v:
                            print(f"{TAB*2}- {u}:")
                            for a, b in v.items():
                                print(f"{TAB*3}{a}: {b}")
                        else:
                            print(f"{TAB*2}- {u}: {v}")


def write_file(contents, filepath):
    with open(filepath, 'w') as f:
        for content in contents:
            for line in content:
                f.write(f"{line}\n")

import argparse
import math
import sys
import yaml
import os
import pathlib

from _common  import *
from _helpers import *

import _helpers as H
import _element as E
import _array   as A
import _record  as R
import _package as P


def build_type_tables(src):
    vprint("")
    curr = {}
    for x,y in src.items():
        database[x] = curr[x] = build_type(x, y)
        vprint(f"{Y2H.TAB}PARSED: {x}")
        allow_tag(curr[x])
    return curr


def main(args):

    Y2H.debug = args.debug
    Y2H.verbose = args.verbose
    Y2H.quiet = args.quiet

    for e in args.ext:
        ext_src, _ = parse_file(e, args.input_format, "EXTERN")
        build_type_tables(ext_src)

    src, cnf = parse_file(args.src, args.input_format, "READ")

    if args.convert is not None:
        convert_input(args.convert, src, cnf)
        sys.exit(1)


    if args.force is False:
        if args.vhdl is True:
            filepath = get_filepath(args.output_dir, args.stemname, '.vhd')
            if os.path.exists(filepath):
                eprint(f"ERROR: file '{filepath}' exists. \n"
                       f"Use --force (-f) to  overwrite.")

        if args.sv is True:
            filepath = get_filepath(args.output_dir, args.stemname, '.svh')
            if os.path.exists(filepath):
                eprint(f"ERROR: file '{filepath}' exists. \n"
                       f"Use --force (-f) to  overwrite.")

        if args.tree is True:
            filepath = get_filepath(args.output_dir, args.stemname, '.txt')
            if os.path.exists(filepath):
                eprint(f"ERROR: file '{filepath}' exists. \n"
                       f"Use --force (-f) to  overwrite.")


    for c in args.custom:
        custom_src, _ = parse_file(c, args.input_format, "CUSTOM")
        src.update(custom_src)
    base.update(build_type_tables(src))

    basic = args.basic or (cnf.get('basic_convert_functions', False)
                           or cnf.get('basic_functions', False))

    pkgs = {}
    if args.additional:
        for a in args.additional:
            a = [x.strip() for x in a.split(".")]
            if a[0] not in pkgs:
                pkgs[a[0]] = list()
            if len(a) > 1 and a[1] not in pkgs[a[0]]:
                pkgs[a[0]] += [a[1]]
    else:
        for lib in cnf.get('packages', dict()):
            pkgs.update(lib)

    P.format_package(args.package_name, pkgs, basic, args.compat, args.timestamp)

    # generate VHDL file
    if args.vhdl:
        filepath = get_filepath(args.output_dir, args.stemname, '.vhd')
        write_file([vhdl.decl, vhdl.body], filepath)
        if args.top is True:
            P.format_vhdl_top(args.package_name, args.compat, pkgs)
            filepath = get_filepath(args.output_dir, args.stemname, '-top.vhd')
            write_file([vhdl.top], filepath)


    # generate SystemVerilog file
    if args.sv:
        filepath = get_filepath(args.output_dir, args.stemname, '.svh')
        write_file([svh], filepath)

    # generate type tree
    if args.tree:
        filepath = get_filepath(args.output_dir, args.stemname, '.txt')
        write_file([tree], filepath)

def get_filepath(output_dir, stemname, extension):
    return os.path.join(output_dir, stemname + extension)

if __name__ == '__main__':

    epilog = "Examples of use:\n"
    switches = {}

    h_add = "Add libraries and packages to preamble. Repeat as needed."
    epilog += ("\tpython3 yml2hdl.py -a ieee.numeric_std -a ieee.math_real"
               " source.yml\n")
    switches[('-a', '--add')] = {'dest':'additional',
                                 'action':'append',
                                 'help':h_add}

    h_basic = ("Generate convert functions for standard VHDL types. If no source file nor"
               " stemname are provided, 'yml2hdl_defs' is used as the destination file.")
    switches[('-b', '--basic')] = {'dest':'basic',
                                   'default':False,
                                   'action':'store_true',
                                   'help':h_basic}

    h_ext = ("External YAML file paths. Types in these files will not be added"
             " to the generated files. Repeat as needed.")
    epilog += "\tpython3 yml2hdl.py -e e1.yml -e e2.yml source.yml\n"
    switches[('-e', '--external')] = {'dest':'ext',
                                      'default':[],
                                      'action':'append',
                                      'help':h_ext}

    h_custom = "External YAML file path for customization. Repeat as needed."
    switches[('-c', '--custom')] = {'dest':'custom',
                                    'action':'append',
                                    'default':[],
                                    'help':h_custom}
    h_debug = "Enable debug reports in VHDL generated files."
    switches[('-D', '--debug')] = {'dest':'debug',
                                   'default':False,
                                   'action':'store_true',
                                   'help':h_debug}

    h_dir = "Directory to be used for the generated files."
    s = "--dir" if '--dir' in sys.argv else "--output-dir"
    switches[('-d', s)] = {'dest':'output_dir',
                                 'default':"",
                                 'help':h_dir}

    h_package_name = "Custom package name."
    s = "--pkg-name" if '--pkg-name' in sys.argv else "--package-name"
    switches[('-p', s)] = {'dest':'package_name',
                           'default':None,
                           'help':h_package_name}

    h_tree = "Create custom type tree view only. Can be combined with '-S' and '-V'."
    epilog += "\tpython3 yml2hdl.py -T source.yml #Type tree only\n"
    switches[('-T', '--tree')] = {'dest':'tree',
                                  'action':'store_true',
                                  'default':False,
                                  'help':h_tree}

    h_vhdl = "Create only VHDL files. Can be combined with '-S' and '-T'."
    epilog += "\tpython3 yml2hdl.py -V source.yml #VHDL only\n"
    switches[('-V', '--vhdl')] = {'dest':'vhdl',
                                  'default':False,
                                  'action':'store_true',
                                  'help':h_vhdl}

    h_sv = "Create only SystemVerilog files. Can be combined with '-T' and '-V'."
    epilog += "\tpython3 yml2hdl.py -S source.yml #SystemVerilog only\n"
    switches[('-S', '--systemverilog')] = {'dest':'sv',
                                           'default':False,
                                           'action':'store_true',
                                           'help':h_sv}

    h_verbose = "Enable verbose output printing."
    switches[('-v', '--verbose')] = {'dest':'verbose',
                                     'default':False,
                                     'action':'store_true',
                                     'help':h_verbose}

    h_stemname = ("Specify stemname (filename without extension) to be used for"
                  " generated files.")
    switches[('-s', '--stemname')] = {'dest':'stemname',
                                      'default':"",
                                      'help':h_stemname}

    h_quiet = "Suppress warnings."
    switches[('-q', '--quiet')] = {'dest':'quiet',
                                   'default':False,
                                   'action':'store_true',
                                   'help':h_quiet}

    h_force = "Overwrite output files."
    switches[('-f', '--force')] = {'dest':'force',
                                   'default':False,
                                   'action':'store_true',
                                   'help':h_force}

    h_input = "YML2HDL input format version. Default to latest."
    epilog += "\tpython3 yml2hdl.py -I 0.2 source.yml \n"
    switches[('-I', '--input-format')] = {'dest':'input_format',
                                          'choices': ['0.2', '0.3'],
                                          'default':None,
                                          'help':h_input}

    h_compat = ("Choose style for YML2HDL VHDL functions."
                " None will generate only the new style (width/convert) functions."
                " Once (-F) generates only the old style (len/vectorify/structify)."
                " Twice (-FF) to generate both styles (duplicate code).")
    switches[('-F', '--functions')] = {'dest':'compat',
                                       'default':0,
                                       'action':'count',
                                       'help':h_compat}

    h_top = ("Generates a basic top entity. For now, it is only available for VHDL.")
    switches[('-t', '--top')] = {'dest':'top',
                                 'default':False,
                                 'action':'store_true',
                                 'help':h_top}

    h_convert = ("Convert input file to a different format."
                 " To create a new file forward the standard output."
                 " Use in combination with '-I'.")
    switches[('-C', '--convert')] = {'dest':'convert',
                                     'choices': ['0.3'],
                                     'default':None,
                                     'help':h_convert}

    h_timestamp = "Add timestamps to the generated files (when)."
    switches[('-w', '--timestamp')] = {'dest':'timestamp',
                                       'default':False,
                                       'action':'store_true',
                                       'help':h_timestamp}

    parser = argparse.ArgumentParser(
        "yml2hdl",
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog=epilog)

    for switch in sorted(switches, key=lambda s: s[0].lower()):
        parser.add_argument(*switch, **switches[switch])

    h_src = "Sorce YAML file path"
    parser.add_argument('src',
                        help=("Sorce YAML file path. Optional."
                              " Use '--' for disambiguation."),
                        default=None,
                        nargs='?')


    if len(sys.argv)==1:
        parser.print_usage()
        sys.exit(0)

    args = parser.parse_args()

    if not args.basic and not args.src and not args.convert:
        eprint("ERROR: no output content.")

    if args.input_format is not None and args.src is None:
        eprint("ERROR: input format version specified but no source file provided.")

    if args.input_format is None:
        args.input_format = Y2H.default_input_format_version

    if not args.stemname:
        if args.src:
            basename = os.path.basename(args.src)
            args.stemname = os.path.splitext(basename)[0]
        elif args.package_name:
            args.stemname = args.package_name
        else:
            args.stemname = "yml2hdl_defs"

    if not args.package_name:
        args.package_name = args.stemname

    if not args.output_dir:
        if args.src:
            args.output_dir = pathlib.Path(args.src).parent
        else:
            args.output_dir = "."

    if not args.tree and not args.vhdl and not args.sv:
        args.vhdl = True
        args.sv = True
        args.tree = True

    main(args)

# YML2HDL 

YML2HDL converts types described as YAML files to HDL types. VHDL (more
complete) and SystemVerilog (initial) are partially supported
for now.

## TLDR

See command line help:
```
python3 yml2hdl -h
```


A simple command line example:
```
python3 yml2hdl types.yml
```

A more complete command line example:
```
python3 yml2hdl -a ieee.numeric_std        \ # extra package from ieee library
                -a ieee.std_logic_1164     \ # extra package from ieee library
	        -b                         \ # include basic conversion functions
	        -e some_dir/base.yml       \ # external yml file
	        -e another_dir/another.yml \ # external yml file
	        -c custom.yml              \ # customizing previous defined types
	        -p my_package              \ # custom package name
	        my_types.yml                 # source file
```

In-file configuration example:
```
config:
    basic_functions : off
    packages:
      - my_lib_1: [package_1, package_2]
      - my_lib_2:
        - the_other_package
```

Constant definition example
```
- LEN0: {qualifier: constant , type: integer , value: 2 , ~: a comment}
```

Unconstrained array definition example:
```
- integer_bus: {type: integer, array: open, comment: this is an array of integers}
```

Unconstrained vector definition example:

```
- vector0: {type: logic, range: open}
```

Record definition example:
```
- record0:
  - r0_var0: {type: logic, comment: another comment}
  - r0_var1: {type: logic, range: [LEN0-1, 0]}
```

Nested definition example:
```
- LEN1: {type: integer , value: 6}

- vector1: {type: logic, range: [LEN1-1, 0]}

- record1:
  - r1_var0: {type: logic, length: record0, ~: bit vector with width of record0}
  - r1_var1: {type: vector1}
  - r1_var2: {type: record0}
```

A complete file example:

```
config:
    basic_functions : off
    packages:
      - my_lib_1:
        - package_1
        - package_2
      - my_lib_2: [the_other_package]
```
types:
```
- LEN0: {qualifier: constant , type: integer , value: 2 , comment: a comment}

- LEN1: {type: integer , value: 6}

- unsigned_bus: {type: unsigned, array: [LEN-1, LEN0]}
```

Lastly, YAML specs are respected, therefore both formats bellow are accepted,
which is useful for example to write longer comments.

```
- LEN0: {qualifier: constant , type: integer , value: 2 , comment: a comment}

- LEN1:
    qualifier: constant
    type: integer
    value: 2
    comment: a comment
```




## Introduction


Since VHDL packages can not be used directly on SystemVerilog code and
*vice-versa*, one of the main usages of `yml2hdl` is to provide a layer
of compatibility between data types. That is: types, constants, etc are
described in source YAML files, then VHDL packages and SystemVerilog
headers are generated from those. This is very useful, for example, when
simulating VHDL code with SystemVerilog testbenches.

And because the interface between SystemVerilog and VHDL blocks can only
happen using "simple" types, any data structure has to be flattened to
bits and vector of bits. For SystemVerilog, this is easy to do by
casting, but for VHDL you would need custom functions for each record,
which can easily become a nightmare. This type of function is also
needed, for instance, when storing information in memory.

Therefore, `yml2hdl` did the hard work of building these conversion
functions (record to bit vector, etc) alongside with the custom data
types described in YAML source file. The implementation relies heavily
on VHDL function overload capabilities, providing a simple API:

- `vectorify` or `convert` to go from record to vector

- `structify` or `convert` to go from vector to record 

- `nullify` or `zeroed` to build signals where all values are zero.


## `yml2hdl` command line usage

All options are documented directly in the tool. Please check the help message
(`-h` or `--help`).

## YAML custom types file

YAML was chosen because of being human readable and for its natural
organizational structure. Imitating VHDL `records`, the YAML file can
have up to 2 level structures for types and re-use the types
defined. YML2HDL also allows the use of the auto-generated types and
constants implicitly, although it requires the developer to know their
names beforehand.

### Types

The YAML file should have at least a top structure to hold the list of
types. Any name containing ``type``, ``constant``, or ``hdl`` are
accepted for that. Case is ignored. Symple types can be described as:

```
<name> :
  qualifier: <qualifier>
  type: <type>
  range: <range>
  value: <value>
```

Which also can be represented in the inline form (much more clear when
using complex structures), for example:

```
<name>: {type: <type>, range: <range>}
```

As we can see from the example above, not all parameters are required,
and the script will try to guess the intention of the description. The
parameters can be:

- type: `logic`, basic types (integer, etc), or previous declared types.

  ```
  bit: {type: logic}
  ```

- qualifier: `constant`. In most case it is not required for now, but
  this can change in the future.

  ```
  MY_CONSTANT: {qualifier: constant, type: integer, range: 10}
  ```
  
- range: `open`, or `[left, right]`. If base type was previously
  defined as `open`, the new range will be used:

  ```
  vector0: {type: logic, range: open}
  vector1: {type: vector0, range: [MY_CONSTANT-1, 0]}
  ```

- value: some initial value. Limited usage for now, use with care. Please open
  issues asking for new features for this.

Records are described as list, for example:

  ```yaml
  record0:
    the_bit     : {type: bit}
    the_vector0 : {type: vector0, range: [MY_CONSTANT-1, 0]}
    the_vector1 : {type: vector1}
  ```

Finally, arrays are defined by the field "array" as follows, which should also
be give as range:

  ```yaml
  arrayA: {array: [3, 0], type: vector1}
  arrayB: {array: open, type: vector0, range: [MY_CONSTANT-1, 0]}
  arrayC: {array: [MY_CONSTANT, 10], type: record0}
  ```

And as mentioned above, use of previously defined types are allowed:

  ```yaml
  vector2: {type: record0}
  arrayD: {type: logic, range: [record0, 0]}
  ```

## Extra parameters

Types can be described by some extra fields:

- `~: <comment>`: allows inclusion of comments in the generated
  types. Multiple `~` can be used for multi-line comments.


### Basic configuration of the YAML source file

An optional structure can be used with basic configuration
details. Any name containing `config` is valid, case insensitive. For
now, only two parameters are implemented:

```yaml
config:
  basic_functions: on
  packages:
    lib_1:
      - package_1
      - package_2
    lib_2: [the_other_package]
```

- `basic_functions` controls the creation of the overload
  functions for the basic types. This is needed when separating the
  files in multiple pieces.

- `packages` allows the inclusion of VHDL packages in the generated
  files, as expected. These are just descriptions of VHDL packages and
  are not used as source for the types constructions in anyway. For
  that, see the `--external` option bellow.


## Running the script

Please see `--help` from yml2hdl tool. To execute the script, it should be as simple as:

```bash
$ python3 <yml2hdl dir path>/yml2hdl.py <yml types file path>
```

To split the custom types into multiple files, the `--external` (`-e`)
option is available, meaning that these files just inform yml2hdl the
definition of the types in other `.yml` files, that is, only the types
of the actual source are written to the output, not the `--external`
ones. Use one switch (`-e`) for each file added.

```bash
$ python3 -e <yml src 1> -e <yml src 2> <source yml file path>
```

The output directory can be changed with the `--dir` (`-d`)
option. Filename is still derived from the source filename. 

```bash
$ python3 -d <directory path> <source yml file path>
```

It is also possible to modify the defined types with a custom source
file (only change types that are already present in the source YAML
files). Use one switch (`-c`) for each file added.

```bash
$ python3 -c <customizing yml file path> <source yml file path>
```

Having a custom package name is also possible (names used for package
and file differs)

```bash
$ python3 -p <custom package name> <source yml file path>
```

Controlling if the convert functions for the basic types
will be part of the generated output can also be done by switches: `-b` or `--basic`.

```bash
$ python3 -b <source yml file path>
```

It is also possible to add libraries and packages to a VHDL resultant
file using options (`-a` or `--add`). These replace libraries
described in the soruce YAML file. Libraries and packages are
separated by a dot. Use one switch (`-a`) for each package added.

```bash
$ python3 -a <library.package> <source yml file path>
```


## Tricks

- When using the same types in different repositories, it might happen
that some libraries and packages have different names. It is easy to
overcome this situation with the `--add` option and no filename
synchronization is needed.

- With a smart combination of package name option, basic function
option, and no source YAML file, it is possible to create a package
with just the basic conversion functions.
```bash 
python3 yml2hdl.py -a ieee.numeric -a ieee.std_logic_1164 -b -p my_package
```

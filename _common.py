import sys
import re
import math
from collections import namedtuple

class Y2H:
    default_input_format_version = '0.3'
    debug   = False
    verbose = False
    quiet   = False
    TAB = "   "

HDL_SECTION_ROOTS = ["type", "constant", "hdl"]

allparams = set()


def get_range_size(r):
    if isinstance(r, list):
        rr = [evaluate(r[0]), evaluate(r[1])]
        return max(rr) - min(rr) + 1
    return None


def get_type_bits(ctype, array_range=None, type_range=None):

    if hasattr(ctype, 'range') and isinstance(ctype.range, list) and isinstance(type_range, list):
        raise RuntimeError(f"{ctype.name} type range double defined ({ctype.range}, {type_range}).")

    #!!!! if hasattr(ctype, 'array') and isinstance(ctype.array, list) and isinstance(array_range, list):
    #!!!!     print("#####", ctype)
    #!!!!     raise RuntimeError(f"{ctype.name} array range double defined ({ctype.array}, {array_range}).")

    if any([isinstance(array_range, str), isinstance(type_range, str)]):
        return None

    bits = 1 if ctype.name in basic_vectors and ctype.type is None else ctype.bits

    if bits is not None:

        array_factor = 1 if array_range is None else get_range_size(array_range)
        type_factor = 1 if type_range is None else get_range_size(type_range)
        return array_factor * type_factor * bits

    if isinstance(ctype, Record):

        if array_range is not None or type_range is not None:
            raise f"Record {ctype} cannot accept ranges"

        w = 0
        for member_name, member_type in ctype.members.items():
            member_bits = get_type_bits(member_type)
            if member_bits is None: return None
            w += member_bits
        return w

    if isinstance(ctype, Array):
        array_range = ctype.array if array_range is None else array_range
        type_range = ctype.range if type_range is None else type_range
        return get_type_bits(ctype.type, array_range, type_range)

    if isinstance(ctype, Element):
        type_range = ctype.range if type_range is None else type_range
        return get_type_bits(ctype.type, array_range, type_range)

    raise "Unexpected behavior. Please report code 005."


def get_description_bits(desc):

    if not isinstance(desc, dict):
        raise RuntimeError("Bug: Type description should be done by a dictionary."
                           " Please report and provide YML source files.")

    if 'members' in desc:
        w = 0
        for name, member in desc['members'].items():
            member_bits = get_type_bits(member)
            if member_bits is None: return None
            w += member_bits
        return w

    if 'array' in desc:
        if not isinstance(desc['array'], list): return None
        w = get_type_bits(desc['type'], array_range=desc['array'], type_range=desc['range'])
        if w is None: return None
        return w

    if isinstance(desc['range'], str): return None
    return get_type_bits(desc['type'], type_range=desc['range'])


# element class ----------------------------------------------------------------
params = ['name', 'type', 'value', 'vhdl', 'bits', 'comment', 'qualifier', 'range', 'sv', 'pkd']
Element = namedtuple('Element', params)
allparams.update(params)

def create_element(**desc):
    d = {}
    d['name']      = None
    d['type']      = None
    d['value']     = None
    d['vhdl']      = None
    d['comment']   = ''
    d['qualifier'] = None
    d['range']     = None
    d['sv']        = None
    d['pkd']       = None

    for k,v in desc.items():
        d[k] = v

    if d['range'] == None:
        if hasattr(d['type'], 'range'):
            if isinstance(d['type'].range, str) and d['type'].range.lower() == 'open':
                d['range'] = 'open'

    d['bits'] = get_description_bits(d)

    e = Element(**d)
    return e

def build_element(name, info):
    info_ = {'name':name}
    for k, v in info.items():
        if k is None:
            info_['comment'] = v
        elif k.lower() == "type":
            if isinstance(v, str):
                info_['type'] = database.get(v, None)
            else:
                info_['type'] = v

            if info_['type'] is None:
                eprint(f"ERROR: type '{v}' not found.")

        else:
            info_[k.lower()] = v
    if 'vhdl' not in info_:
        info_['vhdl'] = (name, name)
    if 'sv' not in info_:
        info_['sv'] = name
    return create_element(**info_)

# array class ------------------------------------------------------------------
params = ['name', 'array', 'type', 'vhdl', 'comment', 'range', 'sv', 'pkd', 'bits', 'qualifier']
Array = namedtuple('Array', params)
allparams.update(params)

def create_array(**desc):
    d = {}
    d['name']       = None
    d['type']       = None
    d['vhdl']       = None
    d['comment']    = ''
    d['range']      = None
    d['sv']         = None
    d['pkd']        = None
    d['bits']       = None
    d['qualifier']  = 'type'

    for k,v in desc.items():
        d[k.lower()] = v

    if 'array' not in d:
        raise AttributeError(f'{d["name"]} is not an array')

    d['bits'] = get_description_bits(d)
    a = Array(**d)
    return a

def build_array(name, info):
    info_ = {'name':name}
    for k, v in info.items():
        if k is None:
            info_['comment'] = v
        elif k.lower() == 'type':
            if isinstance(v, str):
                info_['type'] = database[v]
            else:
                info_['type'] = v
        else:
            info_[k.lower()] = v
    if 'vhdl' not in info_:
        info_['vhdl'] = (None, name)
    if 'sv' not in info_:
        info_['sv'] = name
    return create_array(**info_)

# record class -----------------------------------------------------------------
params   = ['name', 'members', 'vhdl', 'comment', 'sv', 'pkd', 'bits']
Record = namedtuple('Record', params)
allparams.update(params)

def create_record(**desc):
    d = {}
    d['name']      = None
    d['vhdl']      = None
    d['comment']   = ''
    d['sv']        = None
    d['pkd']       = True
    d['bits']      = None

    for k,v in desc.items():
        d[k] = v

    if 'members' not in d:
        raise AttributeError(f'{d["name"]} is not a record.')


    d['bits'] = get_description_bits(d)
    r = Record(**d)
    return r

def build_record(name, info):

    if isinstance(info, list):
        members = {}
        # for each element in the record
        for z in info:
            # split element into name and description
            for n, d in z.items():
                d_={}
                # fix fields in description
                for k, v in d.items():
                    if k is None:
                        d_['comment'] = v
                    elif k.lower() == 'type':
                        d_['type'] = database[v]
                    else:
                        d_[k.lower()] = v
        
                members[n] = build_type(n, d_)
                
        d = {'name':name, 'members':members, 'vhdl':(name, None), 'sv':name}

    elif isinstance(info, dict):
        d = {}
        for k,v in info.items():
            if k is None:
                d['comment'] = v
            else:
                d[k.lower()] = v

        if isinstance(d['members'], list):
            aux = {}
            for member_description in d['members']:
                for n, desc in member_description.items(): pass
                aux[n] = build_type(n, desc)
            d['members'] = aux

        d['name'] = name
        d['vhdl'] = (name, None)
        d['sv'  ] = name

    return create_record(**d)

# ------------------------------------------------------------------------------

def build_type(name, description):
    error = []
    try:
        return build_array(name, description)
    except Exception as e:
        error.append(e)
        try:
            return build_element(name, description)
        except Exception as e:
            error.append(e)
            try:
                return build_record(name, description)
            except Exception as e:
                error.append(e)
                if len(error) > 1:
                    msg = f'The errors were:\n{Y2H.TAB}'
                    msg += f"\n{Y2H.TAB}".join(str(x) for x in error)
                else:
                    msg = 'The error was: '
                    msg += f"\n{Y2H.TAB} {error[0]}"

                eprint(f"ERROR: type '{name}' could not be parsed.\n"
                       f"{msg}\n"
                       f"Is the input file format correct?\n"
                       f"See help for '-I' and '-C' command line switches.")


# ------------------------------------------------------------------------------

def factory_allowed():
    exp = {k: v for k, v in math.__dict__.items() if not k.startswith("__")}
    exp.update({'int': int})
    exp.update({'float': float})

    def allow_tag(ctype):
        if((hasattr(ctype, 'type') and ctype.type.name in list(basic_scalars))
           and (hasattr(ctype, 'value') and ctype.value is not None)):
            exp[ctype.name] = evaluate(ctype.value)
        elif ctype.bits is not None:
            exp[ctype.name] = ctype.bits
            vhdl_map[ctype.name] = (f"{ctype.name}'w", "", "")

    def allowed():
        return exp

    return allow_tag, allowed

allow_tag, get_allowed_tags = factory_allowed()


def math_to_hdl(expr, lang):
    split = re.split(f"(?<=[-+*/()])|(?=[-+*/()])", str(expr))

    for j, piece in enumerate(split):
        piece = piece.replace('"', '')
        piece = piece.replace("'", '')
        split[j] = piece

    replacements = {}
    for j, piece in enumerate(split):
        if piece == "(":
            k, m = j, 1
            while m != 0:
                k += 1
                if split[k] == "(":
                    m += 1
                elif split[k] == ")":
                    m -= 1
            if j > 0:
                if lang == "vhdl":
                    replacement = vhdl_map.get(split[j-1].strip(), None)
                else:
                    replacement = sv_map.get(split[j-1].strip(), None)

                if replacement is not None:
                    replacements[j-1] = replacement[0]
                    replacements[j]   = replacement[1]
                    replacements[k]   = replacement[2]

    for j, piece in enumerate(split):
        split[j] = replacements.get(j, piece)

    return "".join(split)


def evaluate(expression, lang=None):
    if isinstance(expression, int) or isinstance(expression, float):
        return expression

    code = compile(expression, "<string>", "eval")
    allowed_tags = get_allowed_tags()

    ok = True
    for name in code.co_names:
        if name not in allowed_tags:
            ok = False
            break
    if ok is True:
        return eval(code, {"__builtins__": {}}, allowed_tags)

    if lang is None:
        if name in database:
            eprint(f"ERROR: {name} is not constrained"
                               " and cannot be used in ranges.")
        else:
            eprint(f"ERROR: tag '{name}' not found in the allowed math tag list.")

    if lang == "vhdl":
        return math_to_hdl(expression, "vhdl")

    if lang == "sv":
        return math_to_hdl(expression, "sv")

def add_width_attributes(expression):
    split = re.split(f"(?<=[-+*/()])|(?=[-+*/()])", str(expression))
    for jj, piece in enumerate(split):
        if piece in database:
            if ((not  hasattr(database[piece], 'value'))
                or (hasattr(database[piece], 'value') and database[piece].value is not None)):
                split[jj] = f"{piece}'w"
                    
    return "".join(split)





# ------------------------------------------------------------------------------

basic_scalars = {}
basic_vectors = {}

element_base = {"name"      : None,
                "bits"      : None,
                "vhdl"      : None,
                "sv"        : None,
                "type"      : None,
                "value"     : None,
                "comment"   : None,
                "qualifier" : None,
                "range"     : None,
                "pkd"       : True}

basic_scalars['natural'] = Element(**{**element_base,
                                      'name' : 'natural',
                                      'bits' : 32,
                                      'vhdl' : ('natural', None),
                                      'sv'   : 'unsigned'})
basic_scalars['integer'] = Element(**{**element_base,
                                      'name' : 'integer',
                                      'bits' : 32,
                                      'vhdl' : ('integer', None),
                                      'sv'   : 'int'})
basic_scalars['real'   ] = Element(**{**element_base,
                                      'name' : 'real',
                                      'bits' : 32,
                                      'vhdl' : ('real' , None),
                                      'sv'   : 'float'})
basic_scalars['float'  ] = Element(**{**element_base,
                                      'name' : 'float',
                                      'bits' : 32,
                                      'vhdl' : ('real' , None),
                                      'sv'   : 'float'})
basic_scalars['longint'] = Element(**{**element_base,
                                      'name' : 'longint',
                                      'bits' : 64,
                                      'vhdl' : ('unsigned(63 downto 0)', 'unsigned(63 downto 0)'),
                                      'sv'   : 'longint'})
basic_scalars['byte'   ] = Element(**{**element_base,
                                      'name' : 'byte',
                                      'bits' : 8 ,
                                      'vhdl' : ('std_logic_vector(7 downto 0)', None),
                                      'sv'   : 'byte'})
basic_scalars['bit'    ] = Element(**{**element_base,
                                      'name' : 'bit',
                                      'bits' : 1 ,
                                      'vhdl' : ('std_logic', None),
                                      'sv'   : 'bit'})


basic_vectors['logic']          = Element(**{**element_base,
                                             'name'  : 'logic',
                                             'vhdl'  : ('std_logic', 'std_logic_vector'),
                                             'sv'    : 'logic'})
basic_vectors['unsigned']       = Element(**{**element_base,
                                             'name'  : 'unsigned',
                                             'vhdl'  : (None, 'unsigned'),
                                             'sv'    : 'logic unsigned',
                                             'range' : "open"})
basic_vectors['signed']         = Element(**{**element_base,
                                             'name'  : 'signed',
                                             'vhdl'  : (None, 'signed'),
                                             'sv'    : 'logic signed',
                                             'range' : "open"})
basic_vectors['integer_vector'] = Element(**{**element_base,
                                             'name'  : 'integer_vector',
                                             'vhdl'  : (None, 'integer_vector'),
                                             'type'  : basic_scalars['integer'],
                                             'sv'    : 'int',
                                             'range' : "open"})


basic_types = {**basic_vectors, **basic_scalars}


# ------------------------------------------------------------------------------
database = {**basic_types}
base = {}


# ------------------------------------------------------------------------------
vhdl = namedtuple('VHDLPackage', ['decl', 'body', 'top'])([], [], [])
svh  = []
tree = []

# ------------------------------------------------------------------------------
vhdl_map = {}
#vhdl_map['log2']      = ("log2(real", "(", "))")
#vhdl_map['int']       = ("integer"  , "(", ")" )
#vhdl_map['float']     = ("real"     , "(", ")" )
vhdl_map['hex']       = ("x"        , '"', '"' )
vhdl_map['bitvector'] = (''         , '"', '"' )
vhdl_map['bit']       = (''         , "'", "'" )

sv_map = {}
sv_map['log2'] = ('$clog2', 0)


# ------------------------------------------------------------------------------

def dprint(*args, **kwargs):
    if Y2H.debug is True:
        print(*args, **kwargs)


def vprint(*args, **kwargs):
    if Y2H.verbose:
        print(*args, **kwargs)


def wprint(*args, **kwargs):
    if not Y2H.quiet:
        print(*args, **kwargs)


def eprint(*args, **kwargs):
    print(*args, **kwargs)
    sys.exit(1)
